<?php

require __DIR__ . '/../vendor/autoload.php';

/** Docs: https://codex.wordpress.org/Editing_wp-config.php & https://developer.wordpress.org/cli/commands/config/set/ */

/*******************************
 *
 * Load env config
 *
 ******************************/
// $dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv = Dotenv\Dotenv::create(__DIR__ . '/../');
$dotenv->load();

// define( 'WP_SITEURL', 'https://xamou-art.test/wp' );
// define( 'WP_HOME', 'https://xamou-art.test' );
/*******************************
 *
 * Stage and production
 *
 ******************************/

// if ( getenv( 'APP_ENV' ) == 'stage' || getenv( 'APP_ENV' ) == 'prod' ) {
//     /** DB master */
//     define( 'DB_NAME',     getenv( 'APP_DB_NAME' ) );
//     define( 'DB_USER',     getenv( 'APP_DB_USER' ) );
//     define( 'DB_PASSWORD', getenv( 'APP_DB_PASS' ) );
//     define( 'DB_HOST',     getenv( 'APP_DB_HOST' ) );

//     /** Redis Object Cache */
//     // define( 'WP_REDIS_BACKEND_HOST', $config['wp']['redis']['host'] );
//     // define( 'WP_REDIS_BACKEND_PORT', $config['wp']['redis']['port'] );
//     // define( 'WP_CACHE_KEY_SALT',     $config['wp']['redis']['prefix'] );

//     /** WP config */
//     define( 'WP_DEBUG', false );

//     // Allow WP CLI file edit
//     if ( ! defined( 'WP_CLI' ) || ! WP_CLI ) {
//         define( 'DISALLOW_FILE_MODS', true );
//     }
// }

/*******************************
 *
 * Local
 *
 ******************************/

// else {
    /** DB */
    define( 'DB_NAME',     getenv( 'APP_DB_NAME' ) );
    define( 'DB_USER',     getenv( 'APP_DB_USER' ) );
    define( 'DB_PASSWORD', getenv( 'APP_DB_PASS' ) );
    define( 'DB_HOST',     getenv( 'APP_DB_HOST' ) );

    /** Debugging */
    define( 'WP_DEBUG',     false );
    define( 'SCRIPT_DEBUG', false );
    define( 'WP_DEBUG_LOG', false );
    define( 'SAVEQUERIES',  false );

    /** Redis Object Cache */
    define( 'WP_CACHE_KEY_SALT', DB_NAME );
// }

/*******************************
 *
 * Shared
 *
 ******************************/

/** Multisite */
// define( 'IS_MULTISITE', false );

// if ( IS_MULTISITE ) {
//     define( 'WP_ALLOW_MULTISITE', false );
//     define( 'MULTISITE', true );
//     define( 'SUBDOMAIN_INSTALL', false );
//     define( 'PATH_CURRENT_SITE', '/' );
//     define( 'SITE_ID_CURRENT_SITE', 1 );
//     define( 'BLOG_ID_CURRENT_SITE', 1 );
//     define( 'DOMAIN_CURRENT_SITE', getenv( 'APP_DOMAIN' ) );
// }

/** Protocol */
// if ( getenv( 'HTTP_X_FORWARDED_PROTO' ) == 'https' ) {
//     $protocol = 'https://';
//     $_SERVER['HTTPS'] = 'on';
// } else {
    $protocol = 'https://';
// }

/** Cookie domain */
define( 'COOKIE_DOMAIN', false );

/** Custom wp-content dir */
define( 'WP_CONTENT_DIR', __DIR__ . '/wp-content' );
define( 'WP_CONTENT_URL', $protocol . getenv( 'APP_DOMAIN' ) . '/wp-content' );

/** DB */
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

/** Salts */
// if ( file_exists( __DIR__ . '/config/salts.php' ) ) {
//     require_once __DIR__ . '/config/salts.php';
// }

/** Default theme */
// define( 'WP_DEFAULT_THEME', $config['app']['name'] );

/** Table prefix */
$table_prefix = getenv( 'APP_DB_PREFIX' ) . '_'; // unique

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/wp/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
